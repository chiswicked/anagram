package dict

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

type ttm struct {
	fix string
	loo string
	mat bool
}

var tt = []struct {
	in       []string
	out      Nodes
	matching []ttm
}{
	{
		in: []string{"a", "ab", "abcd", "abacus"},
		out: Nodes{'a': &Node{
			Valid: true,
			Children: Nodes{'b': &Node{
				Valid: true,
				Children: Nodes{'a': &Node{
					Valid: false,
					Children: Nodes{'c': &Node{
						Valid: false,
						Children: Nodes{'u': &Node{
							Valid: false,
							Children: Nodes{'s': &Node{
								Valid:    true,
								Children: Nodes{},
							}},
						}},
					}},
				}, 'c': &Node{
					Valid: false,
					Children: Nodes{'d': &Node{
						Valid:    true,
						Children: Nodes{},
					}},
				}},
			}},
		}},
	},
	{
		in: []string{"alone", "along"},
		out: Nodes{'a': &Node{
			Valid: false,
			Children: Nodes{'l': &Node{
				Valid: false,
				Children: Nodes{'o': &Node{
					Valid: false,
					Children: Nodes{'n': &Node{
						Valid: false,
						Children: Nodes{'e': &Node{
							Valid:    true,
							Children: Nodes{},
						}, 'g': &Node{
							Valid:    true,
							Children: Nodes{},
						}},
					}},
				}},
			}},
		}},
	},
}

var ttmatch = []struct {
	in       Nodes
	matching []ttm
}{
	{
		in: Nodes{'a': &Node{
			Valid: true,
			Children: Nodes{'b': &Node{
				Valid: false,
				Children: Nodes{'a': &Node{
					Valid: false,
					Children: Nodes{'c': &Node{
						Valid: false,
						Children: Nodes{'u': &Node{
							Valid: false,
							Children: Nodes{'s': &Node{
								Valid:    true,
								Children: Nodes{},
							}},
						}},
					}},
				}},
			}},
		}},
		matching: []ttm{
			{
				fix: "a",
				loo: "",
				mat: true,
			},
			{
				fix: "ab",
				loo: "",
				mat: false,
			},
			{
				fix: "a",
				loo: "b",
				mat: true,
			},
			{
				fix: "a",
				loo: "c",
				mat: true,
			},
			{
				fix: "ab",
				loo: "c",
				mat: false,
			},
			{
				fix: "abacus",
				loo: "",
				mat: true,
			},
			{
				fix: "",
				loo: "cuabas",
				mat: true,
			},
			{
				fix: "",
				loo: "acuabas",
				mat: true,
			},
			{
				fix: "aba",
				loo: "ucs",
				mat: true,
			},
			{
				fix: "aba",
				loo: "cso",
				mat: false,
			},
		},
	},
}

func TestParse(t *testing.T) {
	for _, tc := range tt {
		t.Run("Test", func(t *testing.T) {
			nodes := Parse(tc.in)
			if !cmp.Equal(nodes, tc.out) {
				t.Errorf("diff: %s", cmp.Diff(tc.out, nodes))
			}
		})
	}
}

func TestCanMatch(t *testing.T) {
	for _, tc := range ttmatch {
		t.Run("Test", func(t *testing.T) {
			for _, m := range tc.matching {
				out := tc.in.CanMatch(m.fix, m.loo)
				if m.mat != out {
					t.Errorf("f: \"%v\" l: \"%v\" expected: %v got: %v", m.fix, m.loo, m.mat, out)
				}
			}
		})
	}
}
