package dict

import (
	"regexp"
	"unicode/utf8"
)

type Nodes map[rune]*Node

type Node struct {
	Valid    bool
	Children Nodes
}

// Parse takes a slice of strings and returns a tree with all possible
// letter sequences that make up valid English words
func Parse(words []string) Nodes {
	nodes := Nodes{}
	for _, word := range words {
		nodes.add(word)
	}
	return nodes
}

// add is a helper function to add a word to a tree of letters sequences
func (n Nodes) add(word string) {
	if len(word) == 0 {
		return
	}

	firstRune, firstRuneSize := utf8.DecodeRuneInString(word)
	isLastRune := firstRuneSize == len(word)

	// Node with rune already exists
	if childNode, ok := n[firstRune]; ok {
		// It's the last rune in string, terminate
		if isLastRune {
			childNode.Valid = true
			return
		}
		// Not the last rune, keep on going
		childrenNodes := Nodes(childNode.Children)
		childrenNodes.add(word[firstRuneSize:])
		return
	}

	// Node with rune doesn't exist yet, create one
	newNode := &Node{
		Children: Nodes{},
		Valid:    isLastRune,
	}

	n[firstRune] = newNode
	// It's the last rune in string, terminate
	if isLastRune {
		newNode.Valid = true
		return
	}
	// Not the last rune, keep on going
	childrenNodes := Nodes(newNode.Children)
	childrenNodes.add(word[firstRuneSize:])
}

// CanMatch takes two strings
// startsWith is a fixed sequence of letters (runes)
// looseLetters is a loose collection of letters (runes)
// The function returns true if
//   - startsWith is a valid word (e.g "retort")
//     in this case loooseLetters will be ignored as we already have a match
//   - startsWith plus characters in any order from looseLetters form a valid word
//     (e.g "reto" and "oirsn". "reto" is not a valid word in itself
//     but combined with letters from "oirsn" they can match "retorn" and "retorsion")
//
// The function returns false otherwise
func (n Nodes) CanMatch(startsWith, looseLetters string) bool {
	// Remove non-letters
	startsWith = clearString(startsWith)
	looseLetters = clearString(looseLetters)

	// If only loose letters
	if len(startsWith) == 0 {
		return n.canMatchLoose(looseLetters)
	}

	// Go through startsWith letter sequence to see if it matches
	r, s := utf8.DecodeRuneInString(startsWith)
	isLastRune := s == len(startsWith)
	if childNode, ok := n[r]; ok {
		var newChildren Nodes = childNode.Children
		if isLastRune {
			// It's the last rune in startsWith string...
			if childNode.Valid {
				// ...and it's a valid string
				// so we can return and ignore looseLetters
				return true
			}
			// ...and it's not a valid string
			// so we continue trying to match with looseLetters
			return newChildren.canMatchLoose(looseLetters)
		}
		// Not the last rune, keep on going
		return newChildren.CanMatch(startsWith[s:], looseLetters)
	}
	return false
}

// canMatchLoose is a helper function to iterate through every rune
// in a given string until it finds one that matches one of the node's children
func (n Nodes) canMatchLoose(loose string) bool {
	if len(loose) == 0 {
		return false
	}
	for i, r := range loose {
		looseMinusCurrent := removeRune([]rune(loose), i)
		if n.CanMatch(string(r), string(looseMinusCurrent)) {
			return true
		}
	}
	return false
}

// removeRune takes a slice of runes and an index and returns a new slice of runes
// with the rune at the given index removed
func removeRune(s []rune, i int) []rune {
	if i >= len(s) {
		return s
	}
	return append(s[0:i], s[i+1:]...)
}

// clearString takes a string and returns it with all non-letter characters removed
// ut8 compatible
func clearString(str string) string {
	return regexp.MustCompile(`[^\p{L}]+`).ReplaceAllString(str, "")
}
