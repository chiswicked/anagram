package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/chiswicked/anagram/internal/dict"
)

func main() {
	if len(os.Args) != 2 && len(os.Args) != 3 {
		fmt.Print("Usage:\n\tanagram [startsWith] [looseLetters]\n\n")
		fmt.Print("Examples:\n\tanagram wor dekl\n\t\tThis will return true as it matches \"wore\", \"word\", \"work\", \"worked\", \"world\"\n")
		fmt.Print("\n\tanagram word\n\t\tThis will return true as it matches \"word\"\n")
		fmt.Print("\n\tanagram \"\" rwdo\n\t\tThis will return true as it matches \"do\", \"word\", \"rod\", \"row\", etc.\n")
		fmt.Print("\n\tanagram chor bq\n\t\tThis will return false as neiter combinations \"chor\", \"chorb\", \"chorq\", \"chorbq\", \"chorqb\" is a valid word.\n")
		return
	}

	startsWith := os.Args[1]
	looseLetters := ""
	if len(os.Args) > 2 {
		looseLetters = os.Args[2]
	}

	t := time.Now()
	n := dict.Parse(dict.Words)
	fmt.Printf("Parsed words into tree in %v\n", time.Since(t))

	t = time.Now()
	c := n.CanMatch(startsWith, looseLetters)
	fmt.Printf("Matched startsWith: \"%v\" looseLetters: \"%v\" in %v\n", startsWith, looseLetters, time.Since(t))
	fmt.Println("Result:", c)
}
