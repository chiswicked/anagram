# Anagram Finder

[![Pipeline Status](https://gitlab.com/chiswicked/anagram/badges/master/pipeline.svg)](https://gitlab.com/chiswicked/anagram/pipelines)
[![Coverage Report](https://gitlab.com/chiswicked/anagram/badges/master/coverage.svg)](https://gitlab.com/chiswicked/anagram/commits/master)

## Installation

```bash
$ go install gitlab.com/chiswicked/anagram/cmd/anagram@latest
```
