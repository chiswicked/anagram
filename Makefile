ifndef GITLAB_CI
# Running locally
PROJECT_BASE_PATH	:= $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
PROJECT_NAME		:= $(notdir $(PROJECT_BASE_PATH))
else
# Running on GitLab CI environment
PROJECT_BASE_PATH   := $(GOPATH)/src/gitlab.com/$(CI_PROJECT_PATH)
PROJECT_NAME		:= $(CI_PROJECT_NAME)
endif

PROJECT_BUILD_PATH	:= $(PROJECT_BASE_PATH)/build

# Local targets

.PHONY: all clean install build test cover cover-clean run

all: clean install test build run

clean:
	@echo [clean] removing build artifacts
	@$(GO_BUILD_ENV) go clean -testcache ./...
	@rm -rf $(PROJECT_BUILD_PATH)/$(PROJECT_NAME)

install:
	@echo [install] installing dependencies
	@$(GO_BUILD_ENV) go mod download

build: clean install
	@echo [build] building binary
	$(GO_BUILD_ENV) go build -o $(PROJECT_BUILD_PATH)/$(PROJECT_NAME) -a $(PROJECT_BASE_PATH)/cmd/$(PROJECT_NAME)

test: clean install
	@echo [test] running unit tests
	@$(GO_BUILD_ENV) go test -v ./...

cover: cover-clean
	@echo [cover] generating test coverage report
	@$(GO_BUILD_ENV) go test -coverprofile cover.out ./...
	@$(GO_BUILD_ENV) go tool cover -html=cover.out -o cover.html

cover-clean:
	@echo [cover-clean] removing test coverage artifacts
	@rm -f cover.out cover.html

run:
	@echo [run] executing binary
	@$(PROJECT_BUILD_PATH)/$(PROJECT_NAME)
